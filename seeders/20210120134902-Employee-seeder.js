'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Employees', [
      {
        employee_name: 'Amzar',
        employee_email: 'Amzar@TED.com',
        employee_NIK: 12345,
        company_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        employee_name: 'Krisna',
        employee_email: 'Krisna@TED.com',
        employee_NIK: 67890,
        company_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      ],
      {});
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Employees', null, {});
  }
};
