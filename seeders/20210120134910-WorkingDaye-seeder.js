'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('WorkingDays', [
      {
      week_day: 'Senin',
      week_date: '2021-01-20',
      is_working: true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      week_day: 'Selasa',
      week_date: '2021-01-21',
      is_working: false,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    ],
    {});
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('WorkingDays', null, {});
  }
};
