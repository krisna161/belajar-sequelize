const Company = require('../models').Company
const Employee = require('../models').Employee

module.exports = {
    async readAll(req,res,next) {
        try {
            const employee = await Employee.findAll({
                include: [{
                    model: Company,
                    as: "Company"
                }]
            })
            if(employee) {
                res.status(200).json({
                    status: true,
                    message: "Success read all employee",
                    data: employee
                })
            }
            else {
                res.status(204).json({
                    status: true,
                    message: "No content",
                    data: {}
                })
            }
        }
        catch(error) {
            res.status(500).json ({
                status: false,
                message: "Internal Server Error",
                data: error
            })
        }
    },
    async readOneById(req,res,next) {
        try {
            const employee_id = req.params.id;
            const employee = await Employee.findOne({
                where: {id: employee_id}, 
                include: [{
                    model: Company,
                    as: "Company"
                }]
            })
            if (employee!=null) {
                res.status(200).json({
                    status: true,
                    message: `Success read employee with id ${req.params.id}`,
                    data: employee
                })
            }
            else {
                res.status(200).json({
                    status: false,
                    message: `Employee with ID ${req.params.id} not found!`,
                    data: {}
                })
            }
        }
        catch(error) {
            res.status(500).json ({
                status: false,
                message: "Internal Server Error",
                data: error
            })
        }
    },
    async createEmployee(req,res,next) {
        try {
            const check_employee = await Employee.findOne({where: {employee_email: req.body.employee_email}})
            if(check_employee) {
                // console.log(req.body.employee_email)
                // console.log(check_employee)
                res.status(200).json({
                    status: false,
                    message: `Employee with email ${req.body.employee_email} already exsist!`,
                    data: check_employee
                })
            }
            else {
                const employee = await Employee.create({
                    employee_name: req.body.employee_name,
                    employee_email:req.body.employee_email,
                    employee_NIK: req.body.employee_NIK,
                    company_id: req.body.company_id
                })
                res.status(200).json({
                    status: true,
                    message: `Success create employee with name ${req.body.employee_name}`,
                    data: employee
                })
            }
           
        }
        catch(error) {
            res.status(500).json ({
                status: false,
                message: "Internal Server Error",
                data: error
            })
        }
    },
    async updateEmployee(req,res,next) {
        try {
            const check_employee = await Employee.findByPk(req.params.id)
            if(check_employee) {
                const employee = await Employee.update({
                    employee_name: req.body.employee_name || employee_name,
                    employee_email:req.body.employee_email || employee_email,
                    employee_NIK: req.body.employee_NIK || employee_NIK,
                    company_id: req.body.company_id || employee_id
                },
                {
                    where: {id : req.params.id}
                })
                const update_employee = await Employee.findByPk(req.params.id)
                res.status(200).json({
                    status: true,
                    dataBefore: check_employee,
                    message: `Employee with id ${req.params.id} updated to`,
                    data: update_employee
                })
            }
            else {
                res.status(200).json({
                    status: false,
                    message: `Employee with id ${req.params.id} not found!`,
                    data: {}
                })
            }
        }
        catch(error) {
            res.status(500).json ({
                status: false,
                message: "Internal Server Error",
                data: error
            })
        }
    },
    async deleteEmployee(req,res,next) {
        try {
            const check_employee = await Employee.findByPk(req.params.id)
            if(check_employee) {
                const employee = await Employee.destroy({where: {id: req.params.id}})
                res.status(200).json({
                    status: true,
                    message: `Employee with id ${req.params.id} deleted!`,
                    data: check_employee
                })
            }
            else {
                const check_all_employees = await Employee.findAll()
                if (check_all_employees.length == 0) {
                    res.status(200).json({
                        status: true,
                        message: `All employees already deleted!`,
                        data: {}
                    })
                }
                res.status(200).json({
                    status: false,
                    message: `Employee with id ${req.params.id} not found!`,
                    data: {}
                })
            }
        }
        catch(error) {
            res.status(500).json ({
                status: false,
                message: "Internal Server Error",
                data: error
            })
        }
    }
}