const userModelFromLogin = require('../models').Login
const JWT = require("jsonwebtoken");
const bcrypt = require('bcrypt');
require("dotenv/config");


module.exports = {
    async readAll(req,res,next) {
        try {
            const userLogin = await userModelFromLogin.findAll()
            res.status(200).json({
                status: true,
                data: userLogin})
        }
        catch(error) {
            res.status(500).json (error)
        }
    },
    // async readOneById(req,res,next) {
    //     try {
    //         const company_id = req.params.id;
    //         const company = await Company.findOne({
    //             where: {id: company_id}
    //             , include: [{
    //                 model: Employee,
    //                 as: "employee"
    //             }]
    //         })
    //         if (company!=null) {
    //             res.status(200).json({
    //                 status: true,
    //                 company
    //             })
    //         } else {
    //             res.status(200).json({
    //                 status: false,
    //                 message: `Company with ID ${req.params.id} not found!`,
    //             })
    //         }
    //     }
    //     catch(error) {
    //         res.status(500).json (error)
    //     }
    // },
    async createUser(req,res,next) {
        try {
            const check_userLogin = await userModelFromLogin.findOne({where: {email: req.body.email}})
            if(check_userLogin) {
                // console.log(`Company Name ${req.body.company_name} already exsist`)
                res.status(200).json({
                    status: false,
                    message: `User with Name ${req.body.name} already exsist!`,
                })
            }
            else {
                // console.log(`Company Name ${req.body.company_name} not exsist`)
                 const userLogin = await userModelFromLogin.create({
                    name: req.body.name,
                    email: req.body.email,
                    password: req.body.password
                })
                res.status(200).json({
                    status: true,
                    userLogin
                })
            }
           
        }
        catch(error) {
            res.status(500).json (error)
        }
    },
    async updateUser(req,res,next) {
        try {
            const check_userLogin = await userModelFromLogin.findByPk(req.params.id)
            if(check_userLogin) {
                const user = await userModelFromLogin.update({
                    name: req.body.name || name,
                    email: req.body.email || email,
                    password: req.body.password || password
                },
                {
                    where: {id : req.params.id}
                })
                const check_userUpdated = await userModelFromLogin.findByPk(req.params.id)
                res.status(200).json({
                    status: true,
                    check_userLogin,
                    message: `User with id ${req.params.id} updated to`,
                    check_userUpdated
                })
            }
            else {
                res.status(200).json({
                    status: false,
                    message: `User with ID ${req.params.id} not found!`,
                })
            }
        }
        catch(error) {
            res.status(500).json (error)
        }
    },
    // async deleteCompany(req,res,next) {
    //     try {
    //         const check_companies = await Company.findByPk(req.params.id)
    //         if(check_companies) {
    //             const company = await Company.destroy({where: {id: req.params.id}})
    //             res.status(200).json({
    //                 status: true,
    //                 check_companies,
    //                 message: `Company with ID ${req.params.id} deleted!`
    //             })
    //         }
    //         else {
    //             res.status(200).json({
    //                 status: false,
    //                 message: `Company with ID ${req.params.id} not found!`
    //             })
    //         }
    //     }
    //     catch(error) {
    //         res.status(500).json (error)
    //     }
    // }
}