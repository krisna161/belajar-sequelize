'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class WorkingDay extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      WorkingDay.belongsToMany(models.Employee, {
        through: "EmployeeWorkingDays",
        foreignKey: "workingday_id",
        as: "employees"
      })
      WorkingDay.hasMany(models.EmployeeWorkingDay,{
        foreignKey: "workingday_id",
        as: "employeeWorkingDays"
      })
    }
  };
  WorkingDay.init({
    week_day: DataTypes.STRING,
    week_date: DataTypes.DATE,
    is_working: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'WorkingDay',
  });
  return WorkingDay;
};