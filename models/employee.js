'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Employee extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Employee.belongsTo(models.Company, {
        foreignKey: 'company_id',
        as: 'Company'
      });
      Employee.hasMany(models.EmployeeWorkingDay, {
        foreignKey: 'workingday_id',
        as: 'employeeWorkingDay'
      });
      Employee.belongsToMany(models.WorkingDay, {
        through: "EmployeeWorkingDays",
        foreignKey: "employee_id",
        as: "workingDay"
      })
    }
  };
  Employee.init({
    employee_name: DataTypes.STRING,
    employee_email: DataTypes.STRING,
    employee_NIK: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Employee',
  });
  return Employee;
};