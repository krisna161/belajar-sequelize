var express = require('express');
const { RequestTimeout } = require('http-errors');
var router = express.Router();

const userController = require('../controllers/user');
const passport = require('passport');

/* GET users listing. */
router.get(
    "/",
    passport.authenticate('jwt', { 'session': false }),
    userController.readAll
    ); 
// router.get("/:id", userController.readOneById);
router.post("/", userController.createUser);
router.put("/:id", userController.updateUser);
// router.delete("/:id", userController.deleteUser);


module.exports = router;
