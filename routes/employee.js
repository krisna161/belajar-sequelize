var express = require("express");
var router = express.Router();


const employeeController = require("../controllers/employee");
const passport = require('passport');

/* GET home page. */
router.get(
    "/",
    passport.authenticate('jwt', { 'session': false }),
    employeeController.readAll
    );

router.get(
    "/:id",
    passport.authenticate('jwt', { 'session': false }),
    employeeController.readOneById
    );

router.post(
    "/",
    passport.authenticate('jwt', { 'session': false }),
    employeeController.createEmployee
    );

router.put(
    "/:id",
    passport.authenticate('jwt', { 'session': false }),
    employeeController.updateEmployee
    );

router.delete(
    "/:id",
    passport.authenticate('jwt', { 'session': false }),
    employeeController.deleteEmployee
    );

module.exports = router;

