var express = require("express");
var router = express.Router();


const company = require("../controllers/company");
const passport = require('passport');

const companyController = new company()
/* GET home page. */
router.get(
    "/",
    passport.authenticate('jwt', { 'session': false }),
    companyController.readAll
    );

router.get(
    "/:id",
    passport.authenticate('jwt', { 'session': false }),
    companyController.readOneById
    );

router.post(
    "/",
    passport.authenticate('jwt', { 'session': false }),
    companyController.createCompany
    );

router.put(
    "/:id",
    passport.authenticate('jwt', { 'session': false }),
    companyController.updateCompany
    );

router.delete(
    "/:id",
    passport.authenticate('jwt', { 'session': false }),
    companyController.deleteCompany
    );

module.exports = router;

